import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import DashboardContainer from './containers/DashboardContainer';
import StudentFormContainer from './containers/StudentFormContainer';
import StudentUpdateFormContainer from './containers/StudentUpdateFormContainer';
import LoaderContainer from './containers/LoaderContainer';
import { Router } from 'react-router-dom';
import { Redirect, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Error from './components/Error';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router history={createBrowserHistory()}>
          <Switch>
            <Route exact path={'/'} component={DashboardContainer} />
            <Route exact path={'/add/'} component={StudentFormContainer} />
            <Route exact path={'/update/:id'} component={StudentUpdateFormContainer} />
            <Route exact path={'/error'} component={Error} />
            <Redirect from='*' to='/' />
          </Switch>
        </Router>
        <LoaderContainer />
      </div>
    </Provider>
  );
}

export default App;
