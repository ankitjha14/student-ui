import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    heading: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(5),
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    control: {
        padding: theme.spacing(2),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: 'center'
    },
}));

export default function Error() {
    const classes = useStyles();
    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12} className={classes.heading}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <LocalLibraryIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            STUDENT LIST
                        </Typography>

                    </Toolbar>
                </AppBar>

            </Grid>

            <Grid container justifyContent="center" spacing={6}>
                <Typography variant="h3" className={classes.title}>
                    We are facing some technical issue!! <p/>
                    <Button variant="contained" size="large" color="default" disableElevation href="/">
                    Go to dashboard
                </Button>
                </Typography>
                
            </Grid>
        </Grid >
    );
}
