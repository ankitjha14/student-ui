import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    yes: {
        float: 'right',
        marginRight: theme.spacing(2),
    },
    cancel: {
        float: 'right',
        marginRight: theme.spacing(2),
    }
}));

export default function ConfirmationModal({ open, handleClose, onConfirmation }) {
    const classes = useStyles();
    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);

    const body = (
        <div style={modalStyle} className={classes.paper}>
            <h2 id="simple-modal-title">Confirmation</h2>
            <p id="simple-modal-description">
                Are you sure you want to delete this student?
            </p>
            <button className={classes.yes} onClick={onConfirmation}>
                yes
            </button>
            <button className={classes.cancel} onClick={handleClose}>
                cancel
             </button>
            <ConfirmationModal />
        </div>
    );

    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>
        </div>
    );
}
