import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import StudentCard from './StudentCard';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    heading: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(5),
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    control: {
        padding: theme.spacing(2),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: 'left'
    },
}));

export default function Dashboard({ fetchStudents, deleteStudent, students, startLoader, stopLoader }) {
    const classes = useStyles();
    useEffect(() => {
        startLoader();
        fetchStudents();
        stopLoader();
    }, []);

    const handleDelete = (id) => {
        startLoader();
        deleteStudent(students, id);
        stopLoader();
    };
    
    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12} className={classes.heading}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <LocalLibraryIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            STUDENT LIST
                        </Typography>
                        <Button variant="contained" size="large" color="default" disableElevation href="/add">
                            Add new student
                        </Button>

                    </Toolbar>
                </AppBar>

            </Grid>

            {students && students.length && <Grid item xs={12}>
                <Grid container justifyContent="center" spacing={6}>
                    {students.map((student) => (
                        <Grid key={student.id} item>
                            <StudentCard key={student.id} student={student} removeStudent={handleDelete} />
                        </Grid>
                    ))}
                </Grid>
            </Grid>}
        </Grid>
    );
}
