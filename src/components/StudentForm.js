import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  left: {
    width: "50%",
  },
  right: {
    width: "50%",
  },
  heading: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(5),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  control: {
    padding: theme.spacing(2),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textAlign: 'left'
  },
  image: {
    width: "50%",
    height: 300,
    float: 'center',
    backgroundImage: "url(" + "https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg" + ")"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '50ch',
  },
  addButton: {
    marginTop: theme.spacing(5),
  },
}));

export default function StudentForm({ saveStudent, students, startLoader, stopLoader, formType, student }) {
  const classes = useStyles();
  const history = useHistory();
  const [studentDetails, setStudentDetails] = useState(student)
  const [openAlert, setOpenAlert] = useState(false)
  const [alert, setAlert] = useState({})

  const [errorFlag, setErrorFlag] = useState({
    firstName: false,
    lastName: false,
    imageUrl: false,
    age: false,
    email: false,
    std: false
  });

  useEffect(() => {
    if (formType === "Update" && Object.keys(student).length === 0) {
      history.push('/');
    }
  }, []);

  const isValid = {
    firstName: (value) => value ? true : false,
    lastName: (value) => value ? true : false,
    imageUrl: (value) => true,
    age: (value) => value ? true : false,
    email: (value) => value ? true : false,
    std: (value) => value ? true : false
  };

  const handleChange = (field, event) => {
    const value = event.target.value;
    setStudentDetails({ ...studentDetails, [field]: value })
    if (isValid[field](value)) {
      setErrorFlag({ ...errorFlag, [field]: false })
    } else {
      setErrorFlag({ ...errorFlag, [field]: true })
    }
  }

  const handleSubmit = () => {
    let shouldSubmit = true;
    const errors = {};
    ["firstName", "lastName", "age", "imageUrl", "email", "std"].forEach(field => {
      if (!isValid[field](studentDetails[field])) {
        shouldSubmit = false;
        errors[field] = true;
      }
    })
    if (shouldSubmit) {
      startLoader();
      const response = saveStudent(students, studentDetails);
      if (response) {
        handleOpenAlert("success", `Student successfuly ${formType === "Add" ? "added" : "updated"}`)
        if (formType === "Add") setStudentDetails({});
      }
      stopLoader();

    } else {
      setErrorFlag(errors)
    }
  }

  const handleOpenAlert = (severity, msg) => {
    setAlert({ severity, msg })
    setOpenAlert(true);
  };

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenAlert(false);
  };

  function textField(field, lable) {
    return (<Grid item>
      <TextField
        label={lable}
        id="outlined-margin-normal"
        value={studentDetails[field] || ''}
        className={classes.textField}
        helperText={errorFlag[field] ? "please provide a correct value" : ""}
        margin="normal" variant="outlined"
        onChange={(e) => handleChange(field, e)}
        onBlur={(e) => handleChange(field, e)} />
    </Grid>
    );
  }


  return (

    <Grid container className={classes.root} spacing={3}>
      <Grid item xs={12} className={classes.heading}>
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <LocalLibraryIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              STUDENT LIST
                        </Typography>

            <Button variant="contained" size="large" color="default" disableElevation onClick={() => { history.push(`/`) }}>
              Go to dashboard
            </Button>

          </Toolbar>
        </AppBar>

      </Grid>

      <Grid item justifyContent="center" xs={12}>
        <Grid container justifyContent="center">

          <Grid item justifyContent="center" className={classes.left}>
            {textField("firstName", "First Name")}
            {textField("lastName", "Last Name")}
            {textField("age", "Age")}
            {textField("std", "Std.")}
            {textField("email", "Email Id")}
          </Grid>

          <Grid item justifyContent="center" className={classes.right}>
            <CardMedia
              component="img"
              alt="student photo"
              image={studentDetails.imageUrl}
              title="Contemplative Reptile"
              className={classes.image}
            />
            <Grid item justifyContent="center">
              <TextField
                id="standard-full-width"
                label="Image URL"
                value={studentDetails["imageUrl"] || ''}
                style={{ width: "70%", float: "left", marginTop: "2%" }}
                placeholder="Add image URL to add picture"
                fullWidth
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => handleChange("imageUrl", e)}
                onBlur={(e) => handleChange("imageUrl", e)}
              />
            </Grid>
          </Grid>


        </Grid>
      </Grid>


      <Grid container justifyContent="center" spacing={12} className={classes.addButton}>
        <Button variant="contained" size="large" color="primary" disableElevation onClick={handleSubmit}>
          {formType} STUDENT
          </Button>
      </Grid>
      <Snackbar anchorOrigin={{ vertical: 'top', horizontal: 'center' }} open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={alert.severity}>
          {alert.msg}
        </Alert>
      </Snackbar>
    </Grid>

  );
}

