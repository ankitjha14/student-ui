import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { CircularProgress, Modal } from '@material-ui/core';

const styles = theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.modalBackground,
  },
  progressWrapper: {
    width: 200,
    height: 200,
    borderRadius: 8,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    boxShadow: `0 2px 6px 0 ${theme.modalBoxShadow}`,
  },
});

function Loader({ classes, loaderCount }) {
  const [open, setOpen] = useState(false);
  const timeoutRef = useRef();

  useEffect(() => {
    clearTimeout(timeoutRef.current);
    timeoutRef.current = setTimeout(() => setOpen(loaderCount > 0), 500);
  }, [loaderCount]);

  return (
    <Modal open={open} disableAutoFocus className={classes.modal} data-testid="loader-modal">
      <div className={classes.progressWrapper}>
        <CircularProgress thickness={5} size={60} className={classes.progress} />
      </div>
    </Modal>
  );
}

Loader.propTypes = {
  classes: PropTypes.exact({
    modal: PropTypes.string.isRequired,
    progressWrapper: PropTypes.string.isRequired,
  }).isRequired,
  loaderCount: PropTypes.number.isRequired,
};

export default withStyles(styles)(Loader);
