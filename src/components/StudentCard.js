import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ConfirmationModal from './ConfirmationModal';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  image: {
    height: 200,
    backgroundImage:"url(" + "https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg" + ")"
  },
});

export default function StudentCard({ student, removeStudent }) {
  const classes = useStyles();
  const history = useHistory();
  const [modalState, setModalState] = React.useState(false);

  const handleOpen = () => {
    setModalState(true);
  };

  const handleClose = () => {
    setModalState(false);
  };

  const handleDelete = () => {
    removeStudent(student.id);
  };

  const { firstName, lastName, imageUrl, age, email, std } = student;
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          image={imageUrl}
          title="Contemplative Reptile"
          className={classes.image}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {firstName}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Full Name: {firstName + ' ' + lastName}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Age: {age}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Std.: {std}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Email Id: {email}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={handleOpen}>
          Delete
        </Button>
        <Button size="small" color="primary" onClick={() => {history.push(`/update/${student.id}`)}}>
          Edit
        </Button>
      </CardActions>
      <ConfirmationModal open={modalState} handleClose={handleClose} handleOpen={handleOpen} onConfirmation={handleDelete} />
    </Card>
  );
}
