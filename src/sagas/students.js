import { all, call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_STUDENTS, updateStudents, DELETE_STUDENT, removeStudent, SAVE_STUDENT, addStudent, UPDATE_STUDENT, editStudent } from '../actions/students';

import api from '../helpers/api';
import { loader } from '../helpers/loader';

export function* getStudents() {
  yield* loader(() => call(api.Students.get), function* (students) {
    if (students) yield put(updateStudents(students));
  });
}

export function* deleteStudent(action) {
  yield* loader(() => call(api.Students.delete, action.id), function* (response) {
    if (response == 200) yield put(removeStudent(action.students, action.id));
  });
}

export function* saveStudent(action) {
  yield* loader(() => call(api.Students.add, action.student), function* (student) {
    if (student) yield put(addStudent(action.students, student));
  });
}

export function* updateStudent(action) {
  yield* loader(() => call(api.Students.update, action.student), function* (student) {
    if (student) yield put(editStudent(action.students, student));
  });
}

export default function* studentSaga() {
  yield all([
    takeEvery(FETCH_STUDENTS, getStudents),
    takeEvery(DELETE_STUDENT, deleteStudent),
    takeEvery(SAVE_STUDENT, saveStudent),
    takeEvery(UPDATE_STUDENT, updateStudent)
  ]);
}
