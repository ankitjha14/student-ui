import { all, fork } from 'redux-saga/effects';
import students from './students';

export default function* rootSaga() {
  yield all([fork(students)]);
}
