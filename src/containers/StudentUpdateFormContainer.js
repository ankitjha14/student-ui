import { connect } from 'react-redux';
import { startLoader, stopLoader } from '../actions/loader';
import { updateStudent } from '../actions/students';
import StudentForm from '../components/StudentForm';

export const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    return {
    students: state.students.list || [],
    student: state.students.list ? state.students.list.find(student => student.id === parseInt(id)) : {},
    formType: "Update"
}
};

export const mapDispatchToProps = dispatch => ({
    saveStudent: (students, student) => dispatch(updateStudent(students, student)),
    startLoader: () => dispatch(startLoader()),
    stopLoader: () => dispatch(stopLoader()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StudentForm);
