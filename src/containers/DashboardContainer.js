import { connect } from 'react-redux';
import { startLoader, stopLoader } from '../actions/loader';
import { fetchStudents, deleteStudent } from '../actions/students';
import Dashboard from '../components/Dashboard';

export const mapStateToProps = state => ({
    students: state.students.list || [],
});

export const mapDispatchToProps = dispatch => ({
    fetchStudents: () => dispatch(fetchStudents()),
    deleteStudent: (students, id) => dispatch(deleteStudent(students, id)),
    startLoader: () => dispatch(startLoader()),
    stopLoader: () => dispatch(stopLoader()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Dashboard);
