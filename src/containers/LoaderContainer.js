import { connect } from 'react-redux';

import Loader from '../components/Loader';

export const mapStateToProps = state => ({
  loaderCount: state.loader.loaderCount,
});

export default connect(mapStateToProps)(Loader);
