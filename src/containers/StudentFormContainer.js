import { connect } from 'react-redux';
import { startLoader, stopLoader } from '../actions/loader';
import { saveStudent } from '../actions/students';
import StudentForm from '../components/StudentForm';

export const mapStateToProps = state => ({
    students: state.students.list || [],
    student: {},
    formType: "Add"
});

export const mapDispatchToProps = dispatch => ({
    saveStudent: (students, student) => dispatch(saveStudent(students, student)),
    startLoader: () => dispatch(startLoader()),
    stopLoader: () => dispatch(stopLoader()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StudentForm);
