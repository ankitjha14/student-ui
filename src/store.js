import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootSaga from './sagas';
import appReducer from './reducers';

const rootReducer = (state, action) => {
  if (action.type === 'CLEAR_STORE_STATE') {
    state = undefined;
  }
  return appReducer(state, action);
};

const applyDevToolsMiddleware = () => {
    return window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : noOp => noOp;
  };

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    applyDevToolsMiddleware(),
  ),
);

sagaMiddleware.run(rootSaga);
export default store;
