import createReducer from './helpers';
import { START_LOADER, STOP_LOADER } from '../actions/loader';

const initialState = {
  loaderCount: 0,
};

const start = state => ({
  loaderCount: state.loaderCount + 1,
});

const stop = state => ({
  loaderCount: state.loaderCount - 1,
});

const loader = createReducer(initialState, {
  [START_LOADER]: start,
  [STOP_LOADER]: stop,
});

export default loader;
