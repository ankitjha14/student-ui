import { combineReducers } from 'redux';
import students from './students';
import loader from './loader';

const appReducer = combineReducers({
  students,
  loader
});

export default appReducer;
