import produce from 'immer';
import { REMOVE_STUDENT, UPDATE_STUDENTS, ADD_STUDENT, EDIT_STUDENT } from '../actions/students';
import createReducer from './helpers';

const initialState = {};

const updateStudents = produce((draft, action) => {
  draft.list = action.data;
});

const removeStudent = produce((draft, action) => {
  const { students, id } = action;
  draft.list = students.filter(student => student.id !== id);
});

const addStudent = produce((draft, action) => {
  const { students, student } = action;
  students.push(student);
  draft.list = students;
});

const editStudent = produce((draft, action) => {
  const { students, student } = action;
  const updatedList = students.map(u => u.id !== student.id ? u : student);
  draft.list = updatedList;
});

const students = createReducer(initialState, {
  [UPDATE_STUDENTS]: updateStudents,
  [REMOVE_STUDENT]: removeStudent,
  [ADD_STUDENT]: addStudent,
  [EDIT_STUDENT]: editStudent,
});

export default students;
