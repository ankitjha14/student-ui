import actionCreator from './helper';

export const FETCH_STUDENTS = 'FETCH_STUDENTS';
export const UPDATE_STUDENTS = 'UPDATE_STUDENTS';
export const REMOVE_STUDENT = 'REMOVE_STUDENT';
export const DELETE_STUDENT = 'DELETE_STUDENT';
export const ADD_STUDENT = 'ADD_STUDENT';
export const SAVE_STUDENT = 'SAVE_STUDENT';
export const UPDATE_STUDENT = 'UPDATE_STUDENT';
export const EDIT_STUDENT = 'EDIT_STUDENT';

export const fetchStudents = actionCreator(FETCH_STUDENTS);
export const updateStudents = actionCreator(UPDATE_STUDENTS, 'data');
export const removeStudent = actionCreator(REMOVE_STUDENT, 'students', 'id');
export const deleteStudent = actionCreator(DELETE_STUDENT, 'students', 'id');
export const saveStudent = actionCreator(SAVE_STUDENT, 'students', 'student');
export const addStudent = actionCreator(ADD_STUDENT, 'students', 'student');
export const updateStudent = actionCreator(UPDATE_STUDENT, 'students', 'student');
export const editStudent = actionCreator(EDIT_STUDENT, 'students', 'student');
