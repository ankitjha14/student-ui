/* eslint-disable import/prefer-default-export */
import { put } from 'redux-saga/effects';

import { startLoader, stopLoader } from '../actions/loader';

export function* loader(task, success) {
  yield put(startLoader());
  const data = yield task();
  yield put(stopLoader());

  if (success && data) {
    yield* success(data);
  }
}

export function* effectWithLoader(effect) {
  try {
    yield put(startLoader());
    return yield effect;
  } finally {
    yield put(stopLoader());
  }
}
