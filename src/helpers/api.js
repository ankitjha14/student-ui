
const api = {
    Students: {
        get: () => {
            const url = `http://localhost:8080/students/`;
            return fetch(url, {
                method: 'get',
            })
                .then(response => response.json())
                .catch((e) => console.log("Error while fetching students!!", e));
        },
        delete: (id) => {
            const url = `http://localhost:8080/students/${id}`;
            return fetch(url, {
                method: 'delete',
            })
                .then(response => response.status)
                .catch((e) => console.log("Error while deleting students!!", e));
        },
        add: (student) => {
            const url = `http://localhost:8080/students/`;
            return fetch(url, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(student)
            })
                .then(response => response.status)
                .catch((e) => console.log("Error while adding students!!", e));
        },
        update: (student) => {
            const url = `http://localhost:8080/students/${student.id}`;
            return fetch(url, {
                method: 'put',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(student)
            })
                .then(response => response.status)
                .catch((e) => console.log("Error while updating students!!", e));
        },
    },
};

export default api;
